define( ['can'], function(can){
	return can.Control( "List", 
	{  // constructor - static
		counter : 0,

		defaults : {
			enabled: true,
			view: "listTemplate"
		}
	},{ //prototype
		init: function(){
			this.enabled = this.options.enabled;
			this.constructor.counter++;
		
			this.element.html( can.view( this.options.view, { elements : this.options.model } ) );
		},

		"li click" : function( el ){
			alert( "element clicked" );
		} 
	});
})