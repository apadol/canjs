define( ['can'], function(can){
	return can.Construct( "Item", 
	{  // constructor - static
		counter : 0
	},{ //prototype
		init: function(){
			this.enabled = true;
			this.constructor.counter++;
		},

		disable: function() { 
			this.enabled = false;
			return this.enabled;
		},
	});
})