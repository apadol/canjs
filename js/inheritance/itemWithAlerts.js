define( ['can', "inheritance/item"], function(can, Item){
	return Item( "ItemWithAlerts",
	{ 
		init: function(){
			this._super();
			alert( "counter:" + this.constructor.counter);
		},

		disable: function() { 
			alert("disable");
		},
	});
})