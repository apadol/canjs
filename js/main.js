require.config({
	paths: {
	    "can": "vendors/can",
	    "jquery": "vendors/jquery"
	}
});

require([
		"jquery", 
		"can", 
		"inheritance/item", 
		"inheritance/itemWithAlerts", 
		"list/controler/list",
		"can/construct/super"
	], function( $, can, Item, ItemWithAlerts, itemList ) {

	//inheritance
	itemInstance = new Item();
	console.log( itemInstance );
	//itemWithAlerts = ItemWithAlerts;

	//observer
	/*
	companyInstance1 = new can.Observe({
		name: "IOKI", 
		owner : { 
			firstName : "Jan",
			lastName : "Lipczynski"
		}
	});

	companyInstance2 = new can.Observe({
		name: "Pearson",
		owner : { 
			firstName : "Samuel",
			lastName : "Pearson" 
		}
	})
	*/
	/*

	companyInstance1.bind("change", function( ev, attr, how, newVal, oldVal ){
		alert("how: " + how + " attr: " + attr );
	}).bind( "name",
		function( ev, newVal, oldVal ){
			alert("Name was: " + oldVal + ", and now its: " + newVal);
		}
	).bind( "owner", 
		function( ev, newVal, oldVal ){
			alert("Owner name was: " + oldVal.firstName + oldVal.lastName + ", and now its: " + newVal.firstName + newVal.lastName);
		}
	).bind( "owner.firstName", 
		function( ev, newVal, oldVal ){
			alert("Owner first name was: " + oldVal.firstName + ", and now its: " + newVal.firstName);
		}
	)
	*/

	/*
	companyInstance1.attr( "name", companyInstance1.cname + "-" + companyInstance2.cname );
	companyInstance1.attr( "owner", companyInstance2.owner);
	*/

	//view
	
	/*$("#list").html( can.view("listTemplate",{
		elements : new can.Observe.List([companyInstance1, companyInstance2])
	}) );*/

	// control 
	
	/*listControl = new itemList("#list", {
		model : new can.Observe.List([companyInstance1, companyInstance2])
	})*/
});